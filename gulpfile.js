var gulp = require('gulp');
var babel = require('gulp-babel');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
//var minify = require('gulp-minify');
var react = require('gulp-react');
var css = require('gulp-mini-css');
var webpackUtil = require('webpack');
var webpack = require('webpack-stream');
const HtmlWebPackPlugin = require("html-webpack-plugin");

//var path = {
//  HTML: 'src/index.html',
//  ALL: ['src/js/*.js', 'src/js/**/*.js', 'src/index.html'],
//  JS: ['src/js/*.js', 'src/js/**/*.js'],
//  MINIFIED_OUT: 'build.min.js',
//  DEST_SRC: 'dist/src',
//  DEST_BUILD: 'dist/build',
//  DEST: 'dist'
//};

var path = {
    HTML: 'src/index.html',
    ALL: ['src/index.js', 'src/App.js', 'src/config/*.js', 'src/startscreen/*.js', 'src/index.html'],
    JS: ['node_modules/babel-polyfill/dist/polyfill.js', 'src/config/*.js', 'src/startscreen/*.js', 'src/App.js', 'src/index.js'],
    CSS: ['src/config/*.css', 'src/startscreen/*.css', 'src/App.css', 'src/index.css'],
    MINIFIED_OUT: 'build.js',
    MINIFIED_CSS_OUT: 'build.css',
    DEST_SRC: 'dist/src',
    DEST_BUILD: 'dist/build',
    DEST_PUBLIC: 'public/lunachick-dashboard-chart',
    DEST: 'dist'
};

gulp.task('buildJs', () => gulp.src(path.JS)
  .pipe(react({
    es6module: true
  }))
  .pipe(babel())
  .pipe(concat(path.MINIFIED_OUT))
  .pipe(minify())
  .pipe(gulp.dest(path.DEST_BUILD))
);

gulp.task('buildCss', () => gulp.src(path.CSS)
  .pipe(concat(path.MINIFIED_CSS_OUT))
  .pipe(css({ext:'-min.css'}))
  .pipe(gulp.dest(path.DEST_BUILD))
);

gulp.task('cleanup', () => gulp.src(['dist/build/*'])
  .pipe(clean())
);

gulp.task('moveFiles', () => gulp.src(['dist/build/*-min.*'])
  .pipe(gulp.dest(path.DEST_PUBLIC))
);

gulp.task('default', gulp.series('buildJs'
  ,'buildCss'
  ,'moveFiles'
  ,'cleanup'
));

/*
var flowBabelWebpackPlugin = new FlowBabelWebpackPlugin();

var definePlugin = new webpackUtil.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'production')
});
/*
var uglifyJsPlugin = new webpackUtil.optimize.UglifyJsPlugin({
  minimize: true,
  compress: {
    warnings: false
  },
  output: {
    comments: false,
    semicolons: true,
  }
});
*/
/*
var webpackConfig = {
  output: {
    filename: "bundle.min.js"
  },
  module: {
    rules: [
      {
        // test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      }
    ]
  },
  plugins: [
    definePlugin,
    new uglifyJsPlugin(),
    flowBabelWebpackPlugin
  ]
};
*/
var webpackConfig = {
  output: {
      filename: "bundle.min.js"
  },
  module: {
      rules: [
          {
              test: /\.(js|jsx)$/,
              exclude: /node_modules/,
              use: {
                  loader: "babel-loader"
              }
          },
          {
            test: /\.html$/,
            use: [
              {
                loader: "html-loader"
              }
            ]
          },
          {
              test: /\.(css)$/,
              use: ['style-loader', 'css-loader']
          }
      ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ]
};

gulp.task('tester', () => gulp.src(['src/config/*.js', 'src/startscreen/*.js', 'src/App.js', 'src/index.js'])
  /*.pipe(babel({
    presets: ['@babel/env', '@babel/react', 'minify']
  }))*/
  .pipe(webpack(webpackConfig))
  .pipe(gulp.dest(path.DEST_BUILD))
);