import React, { Component } from 'react';
import Textfield from '@atlaskit/textfield';
import { Label } from '@atlaskit/field-base';
import Button from '@atlaskit/button';
import './ConfigParam.css'

class ConfigParam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paramName: '',
            paramValue: ''
        }
        // this.props.uuid should be used in all names
        this.handleRemoveThis = this.handleRemoveThis.bind(this);
        this.handleAddNewParam = this.handleAddNewParam.bind(this);
    }

    handleRemoveThis() {
        this.props.removeThisParam(this.props.uuidKey);
    }

    handleAddNewParam() {
        this.props.addNewParamAfterThis(this.props.uuidKey);
    }

    render() {
        return (
            <div className="ConfigParamContainer" name={'param-' + this.props.uuidKey}>
                <div className="ConfigParamPartContainer">
                    <Label htmlFor={'name-' + this.props.uuidKey} label="Name" />
                    <Textfield name={'name-' + this.props.uuidKey} width="small" value={this.state.paramName} 
                            onChange={(e) => this.setState({paramName: e.target.value})} isRequired/>
                </div>
                <div className="ConfigParamPartContainer">
                    <Label htmlFor={'value-' + this.props.uuidKey} label="Value" />
                    <Textfield name={'value-' + this.props.uuidKey} width="medium" value={this.state.paramValue}
                            onChange={(e) => this.setState({paramValue: e.target.value})} isRequired/>
                </div>
                <div className="ConfigParamPartContainer">
                    <Button name={'remove-' + this.props.uuidKey} appearance="danger" onClick={this.handleRemoveThis}>Remove</Button>
                </div>
                <div className="ConfigParamPartContainer">
                    <Button name={'add-' + this.props.uuidKey} appearance="primary" onClick={this.handleAddNewParam}>Add parameter</Button>
                </div>
            </div>
        );
    }
}

export default ConfigParam;