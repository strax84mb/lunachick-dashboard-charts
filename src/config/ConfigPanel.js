import React, { Component } from 'react';
import Button from '@atlaskit/button';
import ConfigParam from './ConfigParam';
import ConfigInterval from './ConfigInterval';
import ConfigJql from './JqlPanel';
import uuid from "uuid";
import './ConfigPanel.css';

class ConfigPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            configParams: [],
            configIntervals: [],
            configJqls: []
        };
        this.addEmptyElement = this.addEmptyElement.bind(this);
        this.addConfigParam = this.addConfigParam.bind(this);
        this.addConfigInterval = this.addConfigInterval.bind(this);
        this.addConfigJql = this.addConfigJql.bind(this);
        this.removeUuidFromArray = this.removeUuidFromArray.bind(this);
        this.removeConfigParam = this.removeConfigParam.bind(this);
        this.removeConfigInterval = this.removeConfigInterval.bind(this);
        this.removeConfigJql = this.removeConfigJql.bind(this);
        this.closeConfig = this.closeConfig.bind(this);
    }

    addEmptyElement(originUUID, oldArray) {
        var newArray = [...oldArray];
        var index = 0;
        if (originUUID !== '') {
            for (var i = 0; i < newArray.length; i++) {
                if (newArray[i].props.uuidKey === originUUID) {
                    index = i;
                    break;
                }
            }
            index++;
            newArray.splice(index, 0, undefined);
        }
        return {
            'array': newArray, 
            'index': index
        };
    }

    addConfigParam(originUUID) {
        let result = this.addEmptyElement(originUUID, this.state.configParams);
        var newArray = result['array'];
        var index = result['index'];
        let uuidNew = uuid.v4();
        newArray[index] = <ConfigParam uuidKey={uuidNew} key={uuidNew} 
                addNewParamAfterThis={(originUUID) => this.addConfigParam(originUUID)}
                removeThisParam={(originUUID) => this.removeConfigParam(originUUID)}
                />;
        this.setState({configParams: newArray});
    }

    removeConfigParam(originUUID) {
        if (originUUID !== '') {
            var newArray = this.removeUuidFromArray(originUUID, this.state.configParams);
            this.setState({configParams: newArray});
        }
    }

    addConfigInterval(originUUID) {
        let result = this.addEmptyElement(originUUID, this.state.configIntervals);
        var newArray = result['array'];
        var index = result['index'];
        let uuidNew = uuid.v4();
        newArray[index] = <ConfigInterval uuidKey={uuidNew} key={uuidNew} 
                addNewIntervalAfterThis={(originUUID) => this.addConfigInterval(originUUID)}
                removeThisInterval={(originUUID) => this.removeConfigInterval(originUUID)}
                />;
        this.setState({configIntervals: newArray});
    }

    removeConfigInterval(originUUID) {
        if (originUUID !== '') {
            var newArray = this.removeUuidFromArray(originUUID, this.state.configIntervals);
            this.setState({configIntervals: newArray});
        }
    }

    removeUuidFromArray(originUUID, oldArray) {
        var newArray = [...oldArray];
        var index = 0;
        for (var i = 0; i < newArray.length; i++) {
            if (newArray[i].props.uuidKey === originUUID) {
                index = i;
                break;
            }
        }
        newArray.splice(index, 1);
        return newArray;
    }

    addConfigJql(originUUID) {
        let result = this.addEmptyElement(originUUID, this.state.configJqls);
        var newArray = result['array'];
        var index = result['index'];
        let uuidNew = uuid.v4();
        newArray[index] = <ConfigJql uuidKey={uuidNew} key={uuidNew} 
                addNewJqlAfterThis={(originUUID) => this.addConfigJql(originUUID)}
                removeThisJql={(originUUID) => this.removeConfigJql(originUUID)}
                />;
        this.setState({configJqls: newArray});
    }

    removeConfigJql(originUUID) {
        if (originUUID !== '') {
            var newArray = this.removeUuidFromArray(originUUID, this.state.configJqls);
            this.setState({configJqls: newArray});
        }
    }

    closeConfig() {

    }

    render() {
        return (
            <div className="ConfigPanelMain">
                <div className="ConfigParamPanel">
                    <div className="ConfigPanelTitle">Custom parameters:</div>
                    {this.state.configParams.length > 0 ? (
                        <div className="ConfigPanelContainers">{this.state.configParams}</div>
                    ) : null}
                    {this.state.configParams.length < 1 ? (
                        <div className="ConfigPanelFirstEntry">
                            <Button onClick={() => this.addConfigParam('')} appearance="primary">Add parameter</Button>
                        </div>
                    ) : null}
                </div><br/>
                <div className="ConfigJqlPanel">
                    <div className="ConfigPanelTitle">JQL queries:</div>
                    {this.state.configJqls.length > 0 ? (
                        <div className="ConfigPanelContainers">{this.state.configJqls}</div>
                    ) : null}
                    {this.state.configJqls.length < 1 ? (
                        <div className="ConfigPanelFirstEntry">
                            <Button onClick={() => this.addConfigJql('')} appearance="primary">Add JQL query</Button>
                        </div>
                    ) : null}
                </div><br/>
                {this.props.chartType === 'line' ? (
                    <div className="ConfigIntervalsPanel">
                        <div className="ConfigPanelTitle">Intervals:</div>
                        {this.state.configIntervals.length > 0 ? (
                            <div className="ConfigPanelContainers">{this.state.configIntervals}</div>
                        ) : null}
                        {this.state.configIntervals.length < 1 ? (
                            <div className="ConfigPanelFirstEntry">
                                <Button onClick={() => this.addConfigInterval('')} appearance="primary">Add interval</Button>
                            </div>
                        ) : null}
                    </div>
                ) : null}
                <div className="ConfigPanelFirstEntry">
                    <Button onClick={this.closeConfig} appearance="primary">Close configuration screen</Button>
                </div>
            </div>
        );
    }
}

export default ConfigPanel;