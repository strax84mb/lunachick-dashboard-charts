import React, { Component } from 'react';
import { Label } from '@atlaskit/field-base';
import { DatePicker } from '@atlaskit/datetime-picker';
import Button from '@atlaskit/button';
import './ConfigInterval.css'

class ConfigInterval extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fromDate: (new Date()).toISOString().substr(0, 10),
            toDate: (new Date()).toISOString().substr(0, 10),
            fromDateDOM: <DatePicker id={'from-' + this.props.uuidKey} 
                placeholder="Input start date"
                onChange={(e) => this.setState({fromDate: e})} />,
            toDateDOM: <DatePicker id={'to-' + this.props.uuidKey} 
                placeholder="Input end date"
                onChange={(e) => this.setState({toDate: e})} />
        }
        
        this.handleRemoveThis = this.handleRemoveThis.bind(this);
        this.handleAddNewParam = this.handleAddNewParam.bind(this);
    }

    handleRemoveThis() {
        this.props.removeThisInterval(this.props.uuidKey);
    }

    handleAddNewParam() {
        this.props.addNewIntervalAfterThis(this.props.uuidKey);
    }

    render() {
        return (
            <div className="ConfigIntervalContainer" name={'interval-' + this.props.uuidKey}>
                <div className="ConfigIntervalPartContainer">
                    <Label htmlFor={'react-select-from-' + this.props.uuidKey + '--input'} label="Start date:" />
                    {this.state.fromDateDOM} 
                </div>
                <div className="ConfigIntervalPartContainer">
                    <Label htmlFor={'to-' + this.props.uuidKey} label="End date:" />
                    {this.state.toDateDOM}
                </div>
                <div className="ConfigIntervalButtonPartContainer">
                    <Button name={'remove-' + this.props.uuidKey} appearance="danger" onClick={this.handleRemoveThis}>Remove</Button>
                </div>
                <div className="ConfigIntervalButtonPartContainer">
                    <Button name={'add-' + this.props.uuidKey} appearance="primary" onClick={this.handleAddNewParam}>Add Interval</Button>
                </div>
            </div>
        );
    }
}

export default ConfigInterval;