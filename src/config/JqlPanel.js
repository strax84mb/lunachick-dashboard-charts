import React, { Component } from 'react';
import { Label } from '@atlaskit/field-base';
import Button from '@atlaskit/button';
import Textfield from '@atlaskit/textfield';
import './JqlPanel.css';

class JqlPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            queryName: undefined,
            query: undefined
        };
        this.handleRemoveThis = this.handleRemoveThis.bind(this);
        this.handleAddNewParam = this.handleAddNewParam.bind(this);
    }

    handleRemoveThis() {
        this.props.removeThisJql(this.props.uuidKey);
    }

    handleAddNewParam() {
        this.props.addNewJqlAfterThis(this.props.uuidKey);
    }

    render() {
        return (
            <div className="ConfigJqlContainer" name={'query-' + this.props.uuidKey}>
                <div className="ConfigJqlPartContainer">
                    <Label htmlFor={'name-' + this.props.uuidKey} label="JQL query name:" />
                    <Textfield name={'name-' + this.props.uuidKey} width="small" value={this.state.queryName} 
                            onChange={(e) => this.setState({queryName: e.target.value})} isRequired/>
                </div>
                <div className="ConfigJqlPartContainer">
                    <Label htmlFor={'jql-' + this.props.uuidKey} label="JQL query name:" />
                    <Textfield name={'jql-' + this.props.uuidKey} width="xlarge" value={this.state.query} 
                            onChange={(e) => this.setState({query: e.target.value})} isRequired/>
                </div>
                <div className="ConfigJqlPartContainer">
                    <Button name={'remove-' + this.props.uuidKey} appearance="danger" onClick={this.handleRemoveThis}>Remove</Button>
                </div>
                <div className="ConfigJqlPartContainer">
                    <Button name={'add-' + this.props.uuidKey} appearance="primary" onClick={this.handleAddNewParam}>Add JQL</Button>
                </div>
            </div>
        );
    }
}

export default JqlPanel;