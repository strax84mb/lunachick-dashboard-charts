import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ChooseChart from './startscreen/ChooseChart';
import ConfigPanel from './config/ConfigPanel';
import './App.css';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            chartChosen: false,
            chosenChart: "none",
            showConfigPanel: false
        };
        this.startConfiguration = this.startConfiguration.bind(this);
    }

    startConfiguration(chart) {
        this.setState({
            chartChosen: true,
            chosenChart: chart,
            showConfigPanel: true
        });
    }


    render() {
        return (
            <div className="App">
                {!this.state.chartChosen ? <ChooseChart chooseChartType={(chart) => this.startConfiguration(chart)}/> : null}
                {this.state.showConfigPanel ? <ConfigPanel chartType={this.state.chosenChart} /> : null}
            </div>
        );
    }
}
  
export default App;
