import React, { Component } from 'react';
import Button from '@atlaskit/button';
import './ChooseCharts.css';

class ChooseCharts extends Component {
    constructor(props) {
        super(props);
        this.chooseChart = this.chooseChart.bind(this);
    }

    chooseChart(chart) {
        this.props.chooseChartType(chart);
    }

    render() {
        return (
            <div className="ChooseCharts">
                <p className="ChooseChartsParagraph">Choose chart:</p>
                <Button appearance="primary" className="ChooseChartsButton" onClick={() => this.chooseChart("pie")}>Pie</Button>
                <Button appearance="primary" className="ChooseChartsButton" onClick={() => this.chooseChart("line")}>Line</Button>
            </div>
        );
    }
}

export default ChooseCharts;